// MOCK DATABASE
let posts = [];

// count variable that will server as the post ID
let count = 1;


// ADD POST DATA
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
    // prevent the page from loading
    e.preventDefault();

    posts.push({
        id: count,
        title: document.querySelector('#txt-title').value,
        body: document.querySelector('#txt-body').value
    })

    // purpose of Increment the id value in count variable
    count++;

    showPosts(posts);
    alert('Successfully added.');

});


// SHOW POST
const showPosts = (posts) => {

    // Sets the html structure to display new posts
    let postEntries = '';
    posts.forEach((post) => {
        postEntries += `
        <div id="post-${post.id}">
          <h3 id="post-title-${post.id}">${post.title}</h3>
          <p id="post-body-${post.id}">${post.body}</p>
          <button onclick="editPost('${post.id}')">Edit</button>
          <button onclick="deletePost('${post.id}')">Delete</button>
        </div>
        `;
    })

    document.querySelector('#div-post-entries').innerHTML = postEntries
};


// EDIT POST
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body
};


// UPDATE POST
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
    e.preventDefault();

    /*
      posts[2].id = 3;  1
      posts[2].title = Transformers;
      posts[2].body = Sci Fi;

      text-edit-id = 3;  1
      txt-edit-title = Anabelle;
      txt-edit-body = Horror;
      posts[2].id = 3 or 1 === #txt-edit-id.value = 3  1

      posts.length = 3;
      posts[i].id == 0;
    */

    for (let i = 0; i < posts.length; i++) {

        // this is the condition in edit post[0].id = 1 === #txt-edit-id.value = 1
        if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;

            showPosts(posts);
            alert('Successfully updated.');

            break;
        }
    }
});


// ACTIVITY SOULUTION

// DELETE POST

const deletePost = (id) => {
    for (let i = 0; i < posts.length; i++) {
        if (posts[i].id.toString() === id) {
            posts.splice(i, 1);
            showPosts(posts);
            alert('Successfully deleted.');
        }
    }
}